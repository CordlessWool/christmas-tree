# Christmas tree Implementation for Xillings FPGA

The Project implement a VGA Port on Xillinx FPGA and creates a Pixel matrix in C code to give it to the FPU. Snow detect green fields, so snow laying down on the tree and the ground. The amount of snow and the size of the tree can be defined via switches.
![Screen Image](IMG_20171220_122929.jpeg)
