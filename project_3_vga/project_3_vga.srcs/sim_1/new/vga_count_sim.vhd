----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.11.2017 16:27:39
-- Design Name: 
-- Module Name: vga_count_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vga_count_sim is
--  Port ( );
end vga_count_sim;

architecture Behavioral of vga_count_sim is
    
    signal vgaclk_t: STD_LOGIC := '0';
    signal hsync_t: STD_LOGIC  := '0';
    signal vsync_t: STD_LOGIC := '0';
    signal blanking_t: STD_LOGIC := '0';
    signal color_t: STD_LOGIC_VECTOR(11 downto 0); 
    
    component VGA_IP_v2_0_S00_AXI is 
        port (
            vgaclk : in STD_LOGIC;
            hsync: out STD_LOGIC;
            vsync: out STD_LOGIC;
            blanking: out STD_LOGIC;
            color: out STD_LOGIC_VECTOR(11 downto 0) 
        );
    end component VGA_IP_v2_0_S00_AXI;
     

begin


        vgaclk_t <= not vgaclk_t after 400ns;

C1:  VGA_IP_v2_0_S00_AXI port map(vgaclk => vgaclk_t, hsync => hsync_t, vsync => vsync_t, blanking => blanking_t, color => color_t);
--C1:  VGA_IP_v2_0_S00_AXI port map(vgaclk, hsync, vsync, blanking, color);
end Behavioral;
