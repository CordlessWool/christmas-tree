----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.11.2017 15:35:45
-- Design Name: 
-- Module Name: VGA_IP_v2_0_S00_AXI - arch_imp
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VGA_IP_v2_0_S00_AXI is
    Port (  vgaclk : in STD_LOGIC;
            hsync: out STD_LOGIC;
            vsync: out STD_LOGIC;
            blanking: out STD_LOGIC;
            color: out STD_LOGIC_VECTOR(11 downto 0) 
          );
end VGA_IP_v2_0_S00_AXI;

architecture arch_imp of VGA_IP_v2_0_S00_AXI is
            
            
            CONSTANT  hcnt_a: INTEGER  := 639;  -- blanking  start
            CONSTANT  hcnt_b: INTEGER  := 670;  -- h-sync  start
            CONSTANT  hcnt_c: INTEGER  := 759;  -- h-sync  end
            CONSTANT  hcnt_d: INTEGER  := 792;  -- h length
            CONSTANT  hcnt_v: INTEGER  := 699;  -- next  line
            CONSTANT  vcnt_e: INTEGER  := 479;  -- v blanking
            CONSTANT  vcnt_f: INTEGER  := 490;  -- v-sync  start
            CONSTANT  vcnt_g: INTEGER  := 492;  -- v-sync  end
            CONSTANT  vcnt_h: INTEGER  := 524;  -- v length
                
            
            signal hcnt: integer := 0;
            signal vcnt: integer := 0;
            signal blank: STD_LOGIC := '0';
   
begin

hcounter:    process (vgaclk)
            
            begin
                if rising_edge(vgaclk) then
                    if hcnt >= hcnt_d then
                        hcnt <= 0;
                    else
                        hcnt <= hcnt +1;
                    end if;
                end if;
            end process hcounter;
            
            
vcounter:    process (vgaclk)
                        
            begin
                if rising_edge(vgaclk) and hcnt = hcnt_v then
                    if vcnt >= vcnt_h then
                        vcnt <= 0;
                    else
                        vcnt <= vcnt +1;
                    end if;
                end if;
            end process vcounter;            


set_hsync: process (hcnt)
    begin
        if(hcnt >= hcnt_b and hcnt <= hcnt_c) then
            hsync <= '1';
        else
            hsync <= '0';
        end if;
    end process set_hsync;         

set_vsync: process (vcnt)
    begin
        if(vcnt >= vcnt_f and vcnt <= vcnt_g) then
            vsync <= '1';
        else
            vsync <= '0';        
        end if;
    end process set_vsync;

set_blanking: process (hcnt, vcnt)
    begin
        if(vcnt >= vcnt_e or hcnt >= hcnt_a) then
            blank <= '1';
        else
            blank <= '0';
        end if;
    end process set_blanking;

blanking <= blank;


process (vgaclk)
begin
    if rising_edge(vgaclk) then
        case blank is
            when '0' => color <= "110010010000";
            when others => color <= "000000000000";
        end case;
    end if;
end process;

end arch_imp;